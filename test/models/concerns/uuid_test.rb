require 'test_helper'

class UuidTest < ActiveSupport::TestCase
  class UuidTestModel
    extend ActiveModel::Callbacks
    include ActiveModel::Validations
    include ActiveModel::Validations::Callbacks
    include Uuid
    attr_accessor :uuid
  end

  setup do
    @model = UuidTestModel.new
  end

  test "uuid is generated before validation" do
    @model.valid?
    assert_not_nil @model.uuid
  end

  test "uuid is the same after re-validating" do
    @model.valid?
    uuid = @model.uuid
    @model.valid?
    assert_equal @model.uuid, uuid
  end
end
