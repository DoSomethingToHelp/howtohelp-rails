module Uuid
  extend ActiveSupport::Concern

  included do
    before_validation :generate_uuid, if: proc { self.uuid.nil? }
    validates :uuid, presence: true
  end

  private

    def generate_uuid(guid = SecureRandom.uuid)
      self.uuid = guid
    end
end
